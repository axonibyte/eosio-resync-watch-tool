package io.blocksmith.eosio.resyncwatchtool.net;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.blocksmith.eosio.resyncwatchtool.persistent.Logger;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HTTPReader {
  private OkHttpClient client = null;
  private Request request = null;

  /**
   * Null constructor.
   */
  public HTTPReader(String url) {
    client = new OkHttpClient();
    request = new Request.Builder()
        .url(url)
        .build();
  }

  /**
   * Retrieve raw data from the webpage.
   * 
   * @return String the raw data
   */
  public String getRaw() {
    try(Response response = client.newCall(request).execute()) {
      return response.body().string();
    } catch(IOException e) {
      Logger.log("Error when reading web page: " + e.getMessage());
    }
    
    return null;
  }
  
  /**
   * Retrieves a JSON object from the webpage's response
   * 
   * @return JSONObject response or <code>null</code> if a problem occurred
   *         when attempting to convert the response to a JSON object
   */
  public JSONObject getJSONObject() {
    try {
      String raw = getRaw();
      if(raw != null) return new JSONObject(raw);
    } catch(JSONException e) {
      Logger.log("Error when retrieving JSON Object from web page: " + e.getMessage());
    } 
    
    return null;
  }
  
  /**
   * Retrieves a JSON array from the webpage's response
   * 
   * @return JSONArray response or <code>null</code> if a problem occurred
   *         when attempting to convert the response to a JSON array
   */
  public JSONArray getJSONArray() {
    try {
      String raw = getRaw();
      if(raw != null) return new JSONArray(raw);
    } catch(JSONException e) {
      Logger.log("Error when retrieving JSON Array from web page: " + e.getMessage());
    } 
    
    return null;
  }

}
