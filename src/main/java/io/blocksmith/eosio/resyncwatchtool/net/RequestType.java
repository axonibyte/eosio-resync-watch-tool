package io.blocksmith.eosio.resyncwatchtool.net;

/**
 * The HTTP request type.
 * 
 * @author Caleb L. Power
 */
public enum RequestType {
  
  /**
   * HTTP DELETE Request
   */
  DELETE,
  
  /**
   * HTTP GET Request
   */
  GET,
  
  /**
   * HTTP POST Request
   */
  POST,
  
  /**
   * HTTP PUT Request
   */
  PUT
}
