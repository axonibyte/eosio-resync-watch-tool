package io.blocksmith.eosio.resyncwatchtool.persistent;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Custom logging.
 * 
 * @author Caleb L. Power
 */
public final class Logger {
  
  private static Set<PrintStream> outputStream = new CopyOnWriteArraySet<>();
  
  static {
    outputStream.add(System.out);
  }
  
  private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
  
  /**
   * Logs a message.
   * 
   * @param message the message
   */
  public static void log(String message) {
    final Date now = Calendar.getInstance().getTime();
    final String prefix = "[" + formatter.format(now) + "] ";
    for(PrintStream out : outputStream)
      out.println(prefix + message);
  }
  
  /**
   * Adds a logger output.
   * 
   * @param printStream the PrintStream output
   */
  public static void addOutput(PrintStream printStream) {
    outputStream.add(printStream);
  }
}
