package io.blocksmith.eosio.resyncwatchtool.node;

import org.json.JSONException;
import org.json.JSONObject;

import io.blocksmith.eosio.resyncwatchtool.net.HTTPReader;
import io.blocksmith.eosio.resyncwatchtool.persistent.Logger;

public class Node {
  
  private static final String ENDPOINT = "/v1/chain/get_info";
  
  private long headBlockNum = -1L;
  private long headBlockTimestamp = -1L;
  private HTTPReader httpReader = null;
  private String chainID = null;
  private String location = null;
  
  public Node(String location) {
    this.httpReader = new HTTPReader(location + ENDPOINT);
    this.location = location;
  }
  
  public synchronized boolean poll() {
    JSONObject response = httpReader.getJSONObject();
    long headBlockNum = -1L;
    long headBlockTimestamp = -1L;
    String chainID = null;
    try {
      headBlockNum = response.getLong("head_block_num");
      headBlockTimestamp = System.currentTimeMillis();
      chainID = response.getString("chain_id");
      this.headBlockNum = headBlockNum;
      this.headBlockTimestamp = headBlockTimestamp;
      this.chainID = chainID;
/*
      System.out.println("location=" + location);
      System.out.println("blocknum=" + headBlockNum);
      System.out.println("timestamp=" + headBlockTimestamp);
      System.out.println("chain=" + chainID);
      System.out.println("--------------------");
*/
      return true;
    } catch(JSONException e) {
      Logger.log(
          String.format("Ran into an issue when polling the node at %1$s: %2$s",
              location,
              e.getMessage()));
    }
    
    return false;
  }
  
  public String getChainID() {
    return chainID;
  }
  
  public long getHeadBlockNum() {
    return headBlockNum;
  }
  
  public long getHeadBlockTime() {
    return headBlockTimestamp;
  }
  
}
