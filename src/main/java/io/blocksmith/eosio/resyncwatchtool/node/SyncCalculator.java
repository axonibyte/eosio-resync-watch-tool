package io.blocksmith.eosio.resyncwatchtool.node;

import java.util.LinkedList;
import java.util.List;

import io.blocksmith.eosio.resyncwatchtool.persistent.Logger;

public class SyncCalculator implements Runnable {
  
  private static final int MAX_LIST_SIZE = 42;
  
  private Node consistentNode = null;
  private Node replayingNode = null;
  
  private List<Long> consistentNodeHeadBlockNums = null;
  private List<Long> consistentNodeHeadBlockTimes = null;
  private List<Long> replayingNodeHeadBlockNums = null;
  private List<Long> replayingNodeHeadBlockTimes = null;
  
  private Thread thread = null;
  
  private SyncCalculator(Node consistentNode, Node replayingNode) {
    this.consistentNode = consistentNode;
    this.replayingNode = replayingNode;
    this.consistentNodeHeadBlockNums = new LinkedList<>();
    this.consistentNodeHeadBlockTimes = new LinkedList<>();
    this.replayingNodeHeadBlockNums = new LinkedList<>();
    this.replayingNodeHeadBlockTimes = new LinkedList<>();
  }
  
  public static SyncCalculator build(Node consistentNode, Node replayingNode) {
    SyncCalculator calculator = new SyncCalculator(consistentNode, replayingNode);
    calculator.thread = new Thread(calculator);
    calculator.thread.setDaemon(true);
    calculator.thread.start();
    return calculator;
  }
  
  @Override public void run() {
    try {
      while(!thread.isInterrupted()) {
        try {
          if(!consistentNode.poll() || !replayingNode.poll()) continue;
          
          if(!consistentNode.getChainID().equals(replayingNode.getChainID())) {
            Logger.log("Chain IDs don't match.");
            System.exit(2);
          }
          
          double consistentVelocity = calculateCurrentVelocity(
              consistentNodeHeadBlockNums, consistentNodeHeadBlockTimes, consistentNode);
          double replayingVelocity = calculateCurrentVelocity(
              replayingNodeHeadBlockNums, replayingNodeHeadBlockTimes, replayingNode);
          
          double projectedTotalTime = (consistentNode.getHeadBlockNum()
              - replayingNode.getHeadBlockNum()
              + (replayingVelocity * replayingNode.getHeadBlockTime())
              - (consistentVelocity * consistentNode.getHeadBlockTime()))
              / (replayingVelocity - consistentVelocity);
          
          double projectedRemainingBlocks = replayingVelocity
              * (projectedTotalTime - replayingNode.getHeadBlockTime());
          
          double projectedTotalBlocks = projectedRemainingBlocks + replayingNode.getHeadBlockNum();
          
          double projectedRemainingTime = projectedTotalTime - replayingNode.getHeadBlockTime();
          
          long projectedRemainingDays = (long)projectedRemainingTime / (1000 * 60 * 60 * 24);
          long projectedRemainingHours = (long)projectedRemainingTime / (1000 * 60 * 60)
              - projectedRemainingDays * 24;
          long projectedRemainingMinutes = (long)projectedRemainingTime / (1000 * 60)
              - projectedRemainingDays * 24 * 60
              - projectedRemainingHours * 60;
          long projectedRemainingSeconds = (long)projectedRemainingTime / 1000
              - projectedRemainingDays * 24 * 60 * 60
              - projectedRemainingHours * 60 * 60
              - projectedRemainingMinutes * 60;
          
          Logger.log(
              String.format("Current replay status:\n"
                  + "Current consistent node head block: %1$d\n"
                  + "Current replaying node head block: %2$d\n"
                  + "Current replay node velocity: %8$f blocks per second\n"
                  + "Projected number of blocks at sync: %3$d\n"
                  + "Projected amount of time remaining: %4$dd %5$dh %6$dm %7$ds\n",
                  consistentNode.getHeadBlockNum(),
                  replayingNode.getHeadBlockNum(),
                  (long)projectedTotalBlocks,
                  projectedRemainingDays,
                  projectedRemainingHours,
                  projectedRemainingMinutes,
                  projectedRemainingSeconds,
                  replayingVelocity * 1000));
          
        } catch(Exception e) {
          e.printStackTrace();
        }
        
        Thread.sleep(3000L);
      }
    } catch(InterruptedException e) { }
  }
  
  private double calculateCurrentVelocity(
      List<Long> blockNums,
      List<Long> blockTimes,
      Node node) {
    
    while(blockNums.size() >= MAX_LIST_SIZE)
      blockNums.remove(0);
    blockNums.add(node.getHeadBlockNum());
    
    while(blockTimes.size() >= MAX_LIST_SIZE)
      blockTimes.remove(0);
    blockTimes.add(node.getHeadBlockTime());
    
    double velocity = 0D;
    for(int i = 1; i < blockNums.size(); i++) {
/*
      System.out.println(
          String.format("velocity += (%1$d - %2$d) / (%3$d - %4$d)",
              blockNums.get(i), blockNums.get(i - 1), blockTimes.get(i), blockTimes.get(i - 1)));
*/
      velocity += (blockNums.get(i) - blockNums.get(i - 1) + 0D) / (blockTimes.get(i) - blockTimes.get(i - 1));
    }
    
    velocity = blockNums.size() > 1 ? velocity / (blockTimes.size() - 1) : blockNums.get(0) / blockTimes.get(0);
/*
    System.out.println("average velocity: " + velocity);
    System.out.println("-------------------");
*/
    return velocity;
  }
  
  public void stop() {
    if(thread != null) thread.interrupt();
  }
  
}
