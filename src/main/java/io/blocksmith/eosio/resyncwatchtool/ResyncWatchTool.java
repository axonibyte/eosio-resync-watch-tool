package io.blocksmith.eosio.resyncwatchtool;

import java.util.Scanner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import io.blocksmith.eosio.resyncwatchtool.node.Node;
import io.blocksmith.eosio.resyncwatchtool.node.SyncCalculator;
import io.blocksmith.eosio.resyncwatchtool.persistent.Logger;

public class ResyncWatchTool {
  
  private static Options options = null;
  
  public static void main(String[] args) {
    options = new Options();
    options.addOption("c", "consistent-node", true, "Consistent node--the node that is up-to-date.");
    options.addOption("r", "replaying-node", true, "Replaying node--the node that is catching up.");
    options.addOption("h", "help", false, "Displays a helpful message.");
    CommandLineParser parser = new DefaultParser();
    CommandLine cli = null;
    
    try {
      cli = parser.parse(options, args);
    } catch(ParseException e) {
      Logger.log("Could not parse command-line arguments: " + e.getMessage());
      printHelp();
      System.exit(1);
    }
    
    final Scanner keyboard = new Scanner(System.in);
    String consistentNodeLocation = null;
    String replayingNodeLocation = null;
    
    if(cli.hasOption("h"))
      printHelp();
    else {
      if(cli.hasOption("c"))
        consistentNodeLocation = cli.getOptionValue("c");
      else do {
        System.out.print("Please enter the API endpoint of the consistent node.\n>");
        consistentNodeLocation = keyboard.nextLine();
      } while(consistentNodeLocation == null
          || consistentNodeLocation.trim().isEmpty());
      
      if(cli.hasOption("r"))
        replayingNodeLocation = cli.getOptionValue("r");
      else do {
        System.out.print("Please enter the API endpoint of the replaying node.\n>");
        replayingNodeLocation = keyboard.nextLine();
      } while(replayingNodeLocation == null
          || replayingNodeLocation.trim().isEmpty());
      
      final SyncCalculator syncCalculator = SyncCalculator.build(
          new Node(consistentNodeLocation),
          new Node(replayingNodeLocation));
      
      String userResponse = null;
      do {
        System.out.print("Please enter 'stop' to stop the program.\n>");
        userResponse = keyboard.nextLine();
      } while(userResponse == null || !userResponse.equalsIgnoreCase("stop"));
      
      keyboard.close();
      syncCalculator.stop();
      System.exit(0);
    }
  }
  
  private static void printHelp() {
    final HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("ResyncWatchTool", options);
  }
  
}
